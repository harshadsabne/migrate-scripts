
update system_config
set value='{
	"appointment_page": [
		{
			"desc": "urgent appointment button",
			"fid": "APPT001",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "exception appointment button",
			"fid": "APPT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "preferred provider autoselect",
			"fid": "APPT003",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Update preferred provider on Appt Confirmation",
			"fid": "APPT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Appt Insurance addition on Appt Confirmation popup",
			"fid": "APPT005",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "New Patient label in Provider listing in Appointments page",
			"fid": "APPT006",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Appointments/Patient page comments",
			"fid": "APPT007",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "35",
			"desc": "Default Findslot Range in Days",
			"fid": "APPT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "21",
			"desc": "Default Reduced Findslot Range in Days",
			"fid": "APPT009",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Do not carry forward previous appt''s notes on rescheduling and followup",
			"fid": "APPT018",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "EHR Supports multiple appointment notes",
			"fid": "APPT019",
			"is_enabled": "false",
			"user_group": "*",
			"overwrite_existing_note": "true"
		},
		{
			"desc": "Does Integration Code prefer the first appointment note only for update in EHR?",
			"fid": "APPT020",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Auto add the R/S note from backend on rescheduling",
			"fid": "APPT021",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Add Visit reason/Chief complaint and Reason description to appointment note automatically",
			"fid": "APPT022",
			"is_enabled": "true",
			"user_group": "*"
		},
				{
			"desc": "Show telehealth confirmation pop-up",
			"fid": "APPT024",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"patient_page": [
		{
			"desc": "workers comp",
			"fid": "PT001",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Demographic Edit",
			"fid": "PT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Duplicate Patient Check",
			"fid": "PT003",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Self pay button",
			"fid": "PT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient status display",
			"fid": "PT005",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 1",
			"fid": "PT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 2",
			"fid": "PT007",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 3",
			"fid": "PT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Portal Invite dropdown option",
			"fid": "PT009",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Alerts dropdown option",
			"fid": "PT010",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Flags dropdown option",
			"fid": "PT011",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Care Team  dropdown option",
			"fid": "PT012",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Family size, income etc box - HJA specific for now",
			"fid": "PT013",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Referring provider display",
			"fid": "PT014",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "PCP display",
			"fid": "PT015",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Payment mode button patient card",
			"fid": "PT035",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Consent to Text",
			"fid": "PT016",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to Call",
			"fid": "PT017",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to email",
			"fid": "PT018",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to language interpreter",
			"fid": "PT019",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Patient Alert",
			"fid": "PT020",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "ELFP specific alerts display on top",
			"fid": "PT021",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Check Insurance Eligibility in Insurance Screen",
			"fid": "PT022",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Get Insurance Details in Insurance Screen",
			"fid": "PT023",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Button for switching insurance priorities(lower,higher)",
			"fid": "PT024",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"fid": "PT027",
			"desc": "Allscripts Notes",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"desc": "Show Appointment notes on details pop-up",
			"fid": "PT038",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Search button on Search patient screen",
			"fid": "PT040",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Addition Description section on the appointment cancellation pop-up",
			"fid": "PT042",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"checkin_page": [
		{
			"default_value": "00,87,88,89",
			"desc": "Checkin - Default appointment statuses to search for initially in Today Appointments Screen",
			"fid": "CHECKIN001",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"default_theme_to_load": "rx"
}'
where key='applicationUIConfig';


	

update system_config
set value='{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "Commercial or Medicare Insurance",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "2",
			"coverageValue": "No Insurance",
			"coverageDescription": "If you do not have insurance or are interested in paying out-of-pocket, please call us at 203-869-1145 so that we can schedule you the old fashioned way.",
			"check": false
		},
		{
			"id": "3",
			"coverageValue": "Workers Comp/MVA",
			"coverageDescription": "We are sorry but we can not schedule a workers compensation appointment online right now. Please contact our Workers Compensation Department at 203-869-1145 and we will be happy to assist you. If you are a new patient, please ask your employer or adjuster to contact us to schedule your initial appointment.",
			"check": false
		},
		{
			"id": "4",
			"coverageValue": "Medicaid",
			"coverageDescription": "If you have Medicaid insurance plan, please call us at 203-869-1145 so that we can schedule you the old fashioned way.",
			"check": false
		}
	],
	"showInsuranceForm": [
		"1"
	],
	"stopBooking": [
		"2",
		"3",
		"4"
	]
}'
where key='INSURANCE_COVERAGE_CONFIG'