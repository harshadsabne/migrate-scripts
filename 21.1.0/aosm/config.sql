
update system_config
set value = '{
	"appointment_page": [
		{
			"desc": "urgent appointment button",
			"fid": "APPT001",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "exception appointment button",
			"fid": "APPT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "preferred provider autoselect",
			"fid": "APPT003",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Update preferred provider on Appt Confirmation",
			"fid": "APPT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Appt Insurance addition on Appt Confirmation popup",
			"fid": "APPT005",
			"is_enabled": "False",
			"user_group": "*"
		},
		{
			"desc": "New Patient label in Provider listing in Appointments page",
			"fid": "APPT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Appointments/Patient page comments",
			"fid": "APPT007",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "35",
			"desc": "Default Findslot Range in Days",
			"fid": "APPT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "21",
			"desc": "Default Reduced Findslot Range in Days",
			"fid": "APPT009",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Display of Recommended Provider not selected dropdown",
			"fid": "APPT010",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Display of Sequential Scheduling button on the Appointments page",
			"fid": "APPT0002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Display of Motor vehicle auto accident checkbox on appointment confirmation screen",
			"fid": "APPT012",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Display of Eliminated Slots button on appointment scheduling screen",
			"fid": "APPT013",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Automate multiple PA selection on selecting MD. If disabled, selection will be manual via a popup",
			"fid": "APPT014",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show/Hide REFERRING PROVIDER FAX NUMBER",
			"fid": "APPT015",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Mandate REFERRING PROVIDER FAX NUMBER",
			"fid": "APPT016",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto-populate chief complaint in CC notes field",
			"fid": "APPT017",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Do not carry forward previous appointments notes on rescheduling",
			"fid": "APPT018",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "EHR Supports multiple appointment notes",
			"fid": "APPT019",
			"is_enabled": "false",
			"user_group": "*",
			"overwrite_existing_note": "false"
		},
		{
			"desc": "Does Integration Code prefer the first appointment note only for update in EHR?",
			"fid": "APPT020",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto add the R/S note from backend on rescheduling",
			"fid": "APPT021",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Add Visit reason/Chief complaint and Reason description to appointment note automatically",
			"fid": "APPT022",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show telehealth confirmation pop-up",
			"fid": "APPT024",
			"is_enabled": "false",
			"user_group": "*"
		}
	],
	"checkin_page": [
		{
			"default_value": "00,10,87,88,89",
			"desc": "Checkin - Default appointment statuses to search for initially in Today Appointments Screen",
			"fid": "CHECKIN001",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Waiting Since,Checkin Progress and Next Action columns under Appointment table on Today Appointments Screen",
			"fid": "CHECKIN002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Print button on Today Appointments Screen",
			"fid": "CHECKIN003",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Pre Survey button on Today Appointments Screen",
			"fid": "CHECKIN004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show Appointment notes on details pop-up",
			"fid": "PT038",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"patient_page": [
		{
			"desc": "workers comp",
			"fid": "PT001",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Demographic Edit",
			"fid": "PT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Duplicate Patient Check",
			"fid": "PT003",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Self pay button",
			"fid": "PT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient status display",
			"fid": "PT005",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 1",
			"fid": "PT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 2",
			"fid": "PT007",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 3",
			"fid": "PT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Portal Invite dropdown option",
			"fid": "PT009",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Alerts dropdown option",
			"fid": "PT010",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Flags dropdown option",
			"fid": "PT011",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Care Team  dropdown option",
			"fid": "PT012",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Family size, income etc box - HJA specific for now",
			"fid": "PT013",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Referring provider display",
			"fid": "PT014",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "PCP display",
			"fid": "PT015",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to Text",
			"fid": "PT016",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to Call",
			"fid": "PT017",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to email",
			"fid": "PT018",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Consent to language interpreter",
			"fid": "PT019",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Patient Alert",
			"fid": "PT020",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "ELFP specific alerts display on top",
			"fid": "PT021",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "0",
			"desc": "Appointment types for which Add to waitlist button should be disabled",
			"fid": "PT022",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Get Details button on Insurance Screen",
			"fid": "PT023",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Insurance Priority Up-Down buttons on Insurance Screen",
			"fid": "PT024",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Check Eligibility button on Insurance Screen",
			"fid": "PT025",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Search in PM system button on Search patient screen",
			"fid": "PT026",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Allscript notes button on patient card",
			"fid": "PT027",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "10,40",
			"desc": "Comma separated list of Appointment statuses for which the Rescheduling button should be displayed on Patients screen",
			"fid": "PT028",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "10,40",
			"desc": "Comma separated list of Appointment statuses for which the Cancel button should be displayed on Patients screen",
			"fid": "PT029",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "10,40",
			"desc": "Comma separated list of Appointments statuses for highlighting appointments as Active (Bold etc) in the appointments list in the Patient screen",
			"fid": "PT030",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "10,40",
			"desc": "Comma separated list of Appointments statuses for which Edit functionality should be enabled in the Appointment Details Popup",
			"fid": "PT031",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Bypass triage workflow at the time of appointment rescheduling",
			"fid": "PT032",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Bypass triage workflow at the time of appointment followup",
			"fid": "PT033",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Add Associated Party button on Insurance Screen",
			"fid": "PT034",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Payment mode button patient card",
			"fid": "PT035",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Search button on Search patient screen",
			"fid": "PT040",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Addition Description section on the appointment cancellation pop-up",
			"fid": "PT042",
			"is_enabled": "true",
			"user_group": "*"
		}
	]
}'
where key='applicationUIConfig';

update system_config
set value = '{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "I have Insurance",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "2",
			"coverageValue": "Self Pay",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "3",
			"coverageValue": "Workers Compensation, Motor Vehicle Accident or Personal Injury",
			"coverageDescription": "We are sorry but we can not schedule these appointments online right now. Please contact our office at 281-955-7577 and we will be happy to assist you.",
			"check": false
		}
	],
	"showInsuranceForm": [
		"1"
	],
	"stopBooking": [
		"3"
	]
}'
where key = 'INSURANCE_COVERAGE_CONFIG'
