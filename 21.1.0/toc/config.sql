update system_config
set value ='{
	"appointment_page": [
		{
			"desc": "Allscripts Notes Button",
			"fid": "PT027",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Add Associated Party button on Insurance Screen",
			"fid": "PT034",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "urgent appointment button",
			"fid": "APPT001",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "exception appointment button",
			"fid": "APPT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "preferred provider autoselect",
			"fid": "APPT003",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Update preferred provider on Appt Confirmation",
			"fid": "APPT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Appt Insurance addition on Appt Confirmation popup",
			"fid": "APPT005",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "New Patient label in Provider listing in Appointments page",
			"fid": "APPT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Appointments/Patient page comments",
			"fid": "APPT007",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "35",
			"desc": "Default Findslot Range in Days",
			"fid": "APPT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "21",
			"desc": "Default Reduced Findslot Range in Days",
			"fid": "APPT009",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Display of Recommended Provider not selected dropdown",
			"fid": "APPT010",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Autoselect multiple provider PAs",
			"fid": "APPT014",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Do not carry forward previous appointment''s notes on rescheduling",
			"fid": "APPT018",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Does EHR support multiple appointment notes structure?",
			"fid": "APPT019",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Does Integration Code prefer the first appointment note only for update in EHR?",
			"fid": "APPT020",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Auto add the R/S note from backend on rescheduling",
			"fid": "APPT021",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Add Visit reason/Chief complaint and Reason description to appointment note automatically",
			"fid": "APPT022",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto-populate chief complaint in CC notes field",
			"fid": "APPT017",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show telehealth confirmation pop-up",
			"fid": "APPT024",
			"is_enabled": "false",
			"user_group": "*"
		}
	],
	"checkin_page": [
		{
			"default_value": "00",
			"desc": "Checkin - Default appointment statuses to search for initially in Today Appointments Screen",
			"fid": "CHECKIN001",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Waiting Since, Checkin Progress and Next Action columns under Appointment table on Today Appointments Screen",
			"fid": "CHECKIN002",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Print button on Today Appointments Screen",
			"fid": "CHECKIN003",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Pre Survey button on Today Appointments Screen",
			"fid": "CHECKIN004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Actions button to bypass encounter on Today Appointments Screen",
			"fid": "CHECKIN005",
			"is_enabled": "false",
			"user_group": "*"
		}
	],
	"patient_page": [
		{
			"desc": "workers comp",
			"fid": "PT001",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Demographic Edit",
			"fid": "PT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Duplicate Patient Check",
			"fid": "PT003",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Self pay button",
			"fid": "PT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient status display",
			"fid": "PT005",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 1",
			"fid": "PT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 2",
			"fid": "PT007",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 3",
			"fid": "PT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Portal Invite dropdown option",
			"fid": "PT009",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alerts dropdown option",
			"fid": "PT010",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Flags dropdown option",
			"fid": "PT011",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Care Team  dropdown option",
			"fid": "PT012",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Family size, income etc box - HJA specific for now",
			"fid": "PT013",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Patient Alert",
			"fid": "PT020",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "ELFP specific alerts display on top",
			"fid": "PT021",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "-1",
			"desc": "Appointment types for which Add to waitlist button should be disabled",
			"fid": "PT022",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Insurance Priority Change Button",
			"fid": "PT024",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "10,80",
			"desc": "Appointment Statuses to allow reschedule",
			"fid": "PT028",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Appointment notes on details pop-up",
			"fid": "PT038",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Payment mode button patient card",
			"fid": "PT035",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Search button on Search patient screen",
			"fid": "PT040",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Addition Description section on the appointment cancellation pop-up",
			"fid": "PT042",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"dashin_theme": {
		"primary_color": "#f47b20",
		"secondary_color": "#ffe5d2",
		"primary_text_color": "white",
		"secondary_text_color": "black",
		"completed_form_color": "green",
		"remaining_form_color": "light green"
	}
}'
where key='applicationUIConfig';

update system_config
set Value='{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "I have Insurance",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "2",
			"coverageValue": "No Insurance",
			"coverageDescription": "Please Enter the Primary Insurance",
			"check": false
		},
		{
			"id": "3",
			"coverageValue": "Worker''s Comp",
			"coverageDescription": "",
			"check": false
		}
	],
	"showInsuranceForm": [
		"1"
	],
	"stopBooking": [
		"2",
		"3"
	]
}'
where Key='INSURANCE_COVERAGE_CONFIG';
