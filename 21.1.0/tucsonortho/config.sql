update system_config
set value ='{
	"appointment_pagae": [
		{
			"desc": "urgent appointment button",
			"fid": "APPT001",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "exception appointment button",
			"fid": "APPT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "preferred provider autoselect",
			"fid": "APPT003",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Update preferred provider on Appt Confirmation",
			"fid": "APPT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Appt Insurance addition on Appt Confirmation popup",
			"fid": "APPT005",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "New Patient label in Provider listing in Appointments page",
			"fid": "APPT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Appointments/Patient page comments",
			"fid": "APPT007",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "35",
			"desc": "Default Findslot Range in Days",
			"fid": "APPT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "21",
			"desc": "Default Reduced Findslot Range in Days",
			"fid": "APPT009",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "0",
			"desc": "Display of Recommended Provider not selected dropdown ( set default value to 1 for making it a required field )",
			"fid": "APPT010",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Display of Sequential Scheduling button on the Appointments page",
			"fid": "APPT011",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Display of Motor vehicle auto accident checkbox on appointment confirmation screen",
			"fid": "APPT012",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Display of Eliminated Slots button on appointment scheduling screen",
			"fid": "APPT013",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Automate multiple PA selection on selecting MD. If disabled, selection will be manual via a popup",
			"fid": "APPT014",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show/Hide REFERRING PROVIDER FAX NUMBER",
			"fid": "APPT015",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Mandate REFERRING PROVIDER FAX NUMBER",
			"fid": "APPT016",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Removing Auto Populated Chief Complaint Notes - (set to true for auto populate)",
			"fid": "APPT017",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Do not carry forward previous appointment''s notes on rescheduling",
			"fid": "APPT018",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Does EHR support multiple appointment notes structure?",
			"fid": "APPT019",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Does Integration Code prefer the first appointment note only for update in EHR?",
			"fid": "APPT020",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto add the R/S note from backend on rescheduling",
			"fid": "APPT021",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Add Visit reason/Chief complaint and Reason description to appointment note automatically",
			"fid": "APPT022",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show telehealth confirmation pop-up",
			"fid": "APPT024",
			"is_enabled": "false",
			"user_group": "*"
		}
	],
	"patient_page": [
		{
			"desc": "Allscript notes button on patient card",
			"fid": "PT027",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "10,40,75,43",
			"desc": "Comma separated list of Appointment statuses for which the Rescheduling button should be displayed on Patient''s screen",
			"fid": "PT028",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "10,43",
			"desc": "Comma separated list of Appointment statuses for which the Cancel button should be displayed on Patient''s screen",
			"fid": "PT029",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Add Associated Party button on Insurance Screen",
			"fid": "PT034",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "workers comp",
			"fid": "PT001",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Demographic Edit",
			"fid": "PT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Duplicate Patient Check",
			"fid": "PT003",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Self pay button",
			"fid": "PT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Payment mode button patient card",
			"fid": "PT035",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient status display",
			"fid": "PT005",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 1",
			"fid": "PT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 2",
			"fid": "PT007",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 3",
			"fid": "PT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Portal Invite dropdown option",
			"fid": "PT009",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alerts dropdown option",
			"fid": "PT010",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Flags dropdown option",
			"fid": "PT011",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Care Team  dropdown option",
			"fid": "PT012",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Family size, income etc box - HJA specific for now",
			"fid": "PT013",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Referring provider display",
			"fid": "PT014",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "PCP display",
			"fid": "PT015",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to Text",
			"fid": "PT016",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to Call",
			"fid": "PT017",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to email",
			"fid": "PT018",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to language interpreter",
			"fid": "PT019",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Patient Alert",
			"fid": "PT020",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "ELFP specific alerts display on top",
			"fid": "PT021",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Check Insurance Eligibility in Insurance Screen",
			"fid": "PT022",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Get Insurance Details in Insurance Screen",
			"fid": "PT023",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Button for switching insurance priorities(lower,higher)",
			"fid": "PT024",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Appointment notes on details pop-up",
			"fid": "PT038",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Bypass triage workflow at the time of appointment rescheduling",
			"fid": "PT032",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Search in PM system button on Search patient screen",
			"fid": "PT026",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Search button on Search patient screen",
			"fid": "PT040",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Addition Description section on the appointment cancellation pop-up",
			"fid": "PT042",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"checkin_page": [
		{
			"default_value": "80",
			"desc": "Checkin - Default appointment statuses to search for initially in Today Appointments Screen",
			"fid": "CHECKIN001",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"default_theme_to_load": "rx"
}' 
where key='applicationUIConfig';

Update system_config
set value = '{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "I have insurance",
			"coverageDescription": "",
			"check": true
		},
		{
			"id": "2",
			"coverageValue": "Self pay",
			"coverageDescription": "",
			"check": true
		},
		{
			"id": "3",
			"coverageValue": "Worker Comp",
			"coverageDescription": "",
			"check": true
		}
	],
	"showInsuranceForm": [
		"1"
	],
	"stopBooking": [
		"2",
		"3"
	]
}'
where key ='INSURANCE_COVERAGE_CONFIG';

update system_config

set value='{
	"screens": [
		{
			"key": "screen1",
			"label": "PINFO",
			"description": "1st screen in the appointment scheduling workflow.",
			"forms": [
				{
					"key": "patientInfo",
					"label": "Patient Information",
					"description": "Form to capture patient information",
					"fields": [
						{
							"key": "patient_fname",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							},
							"tooltip": "Patient''s First Name"
						},
						{
							"key": "patient_mname",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_lname",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							},
							"tooltip": "Patient''s Last Name"
						},
						{
							"key": "patient_dob_month",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "Patient''s Birth Month"
						},
						{
							"key": "patient_dob_day",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "Patient''s Birth Day"
						},
						{
							"key": "patient_dob_year",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "Patient''s Birth Year"
						},
						{
							"key": "patient_gender",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "Patient''s Gender"
						},
						{
							"key": "patient_hphone",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "\\d{10}$"
							},
							"tooltip": "Patient''s Home Phone"
						},
						{
							"key": "patient_email",
							"display": false,
							"i18Key": "",
							"tooltip": "Patient''s email",
							"validations": {
								"required": true,
								"email": true
							}
						},
						{
							"key": "patient_zip",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "^[\\d]+$",
								"minLength": 5,
								"maxLength": 5
							},
							"tooltip": "Patient''s Zip code"
						},
						{
							"key": "patient_language",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_mphone",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_address",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_city",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_state",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_ssn",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_primary_provider_name",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_primary_provider_id",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_primary_care_provider",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_text_notification",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_email_notification",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "tc_hippa",
							"display": false,
							"i18Key": "",
							"validations": {
								"requiredTrue": true
							}
						},
						{
							"key": "captcha",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": true
							}
						}
					]
				}
			]
		},
		{
			"key": "screen2",
			"label": "IINFO",
			"description": "2nd screen in the appointment scheduling workflow.",
			"forms": [
				{
					"key": "insuranceInfo",
					"label": "Insurance Information",
					"description": "Form to capture Insurance information",
					"fields": [
						{
							"key": "primaryInsuranceCompany",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "Primary Insurance"
						},
						{
							"key": "primary_policy_id",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							},
							"tooltip": "Policy Id"
						},
						{
							"key": "primary_group_number",
							"display": true,
							"i18Key": "",
							"validations": {
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							},
							"tooltip": "Group Number"
						},
						{
							"key": "primary_insurance_phone_no",
							"display": true,
							"i18Key": "",
							"tooltip": "Phone Number",
							"validations": {
								"required": false
							}
						},
						{
							"key": "secondary_insurance_option",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "secondaryInsuranceCompany",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "secondary_policy_id",
							"display": true,
							"i18Key": "",
							"validations": {
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							}
						},
						{
							"key": "secondary_group_number",
							"display": true,
							"i18Key": "",
							"validations": {
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							}
						},
						{
							"key": "secondary_insurance_phone_no",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "no_insurance_option",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "work_insurance_option",
							"display": true,
							"i18Key": ""
						}
					]
				}
			]
		},
		{
			"key": "screen3",
			"label": "VISIT.REASON",
			"description": "3rd screen in the appointment scheduling workflow.",
			"forms": [
				{
					"key": "visitReason",
					"label": "Reason for visit",
					"description": "Form to capture reason for visit",
					"fields": [
						{
							"key": "reason",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": true
							}
						}
					]
				}
			]
		},
		{
			"key": "screen4",
			"label": "CLINICAL.DETAILS",
			"description": "4th screen in the appointment scheduling workflow.",
			"forms": [
				{
					"key": "clinicalDetails",
					"label": "Clinical Details",
					"description": "Form to capture clinical details requested by the provkeyer",
					"fields": []
				}
			]
		},
		{
			"key": "screen5",
			"label": "BOOKING",
			"description": "5th screen in the appointment scheduling workflow.",
			"forms": [
				{
					"key": "demographicData",
					"label": "Additional Patient Information ",
					"description": "Form to capture additional patient information",
					"fields": [
						{
							"key": "patient_fname",
							"display": true,
							"disabled": true,
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "First Name"
						},
						{
							"key": "patient_lname",
							"display": true,
							"disabled": true,
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "Last Name"
						},
						{
							"key": "patient_pname",
							"display": true,
							"tooltip": "preferred Name",
							"disabled": false,
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": false
							}
						},
						{
							"key": "patient_dob",
							"display": true,
							"disabled": true,
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "Date Of Birth"
						},
						{
							"key": "patient_gender",
							"display": true,
							"disabled": true,
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "Gender"
						},
						{
							"key": "patient_ssn",
							"display": false,
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": false,
								"minLength": 9,
								"maxLength": 9
							},
							"tooltip": "SSN"
						},
						{
							"key": "patient_language",
							"display": true,
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "Language"
						},
						{
							"key": "patient_location",
							"display": true,
							"formGroupName": "patientAddressGroup",
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "Location"
						},
						{
							"key": "patient_suite",
							"display": true,
							"formGroupName": "patientAddressGroup",
							"i18Key": "",
							"tooltip": "Suite"
						},
						{
							"key": "patient_city",
							"display": true,
							"formGroupName": "patientAddressGroup",
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							},
							"tooltip": "City"
						},
						{
							"key": "patient_state",
							"display": true,
							"formGroupName": "patientAddressGroup",
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							},
							"tooltip": "State"
						},
						{
							"key": "patient_zip",
							"display": true,
							"formGroupName": "patientAddressGroup",
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "^[\\d]+$",
								"minLength": 5
							},
							"tooltip": "Zipcode"
						},
						{
							"key": "patient_mphone",
							"display": true,
							"formGroupName": "patientContactGroup",
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "Mobile Phone Number"
						},
						{
							"key": "patient_hphone",
							"display": true,
							"formGroupName": "patientContactGroup",
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "Home Phone Number"
						},
						{
							"key": "patient_email",
							"display": true,
							"formGroupName": "patientContactGroup",
							"i18Key": "",
							"validations": {
								"required": false,
								"email": true
							},
							"tooltip": "Email"
						},
						{
							"key": "primary_provider",
							"display": true,
							"formGroupName": "providerInformationGroup",
							"i18Key": "",
							"tooltip": "Primary Provider",
							"autocomplete": true
						},
						{
							"key": "referring_provider",
							"display": true,
							"formGroupName": "providerInformationGroup",
							"i18Key": "",
							"validations": {
								"required": true
							},
							"tooltip": "Referring Provider",
							"autocomplete": true
						},
						{
							"key": "patient_text_notification",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "patient_email_notification",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "is_waitlist",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "is_demographics_update",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "is_insurance_update",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "no_insurance_option",
							"display": true,
							"i18Key": ""
						}
					]
				},
				{
					"key": "feedback",
					"label": "Feedback",
					"description": "Form to capture user feedback",
					"fields": []
				}
			]
		},
		{
			"key": "screen6",
			"label": "CONFIRMATION",
			"description": "5th screen in the appointment scheduling workflow.",
			"display": {
				"maps": true
			}
		},
		{
			"key": "LandingPage",
			"label": "Main application search page",
			"description": "First page of self application to search cheif complain, insurance, zipcode, age",
			"forms": [
				{
					"key": "searchPage",
					"label": "Search Information",
					"description": "Form to capture cheif complain, age, insurance and zipcode information",
					"fields": [
						{
							"key": "searchTerm",
							"display": true,
							"i18Key": "",
							"tooltip": "Search Visit Reason",
							"validations": {
								"required": true
							}
						},
						{
							"key": "insuranceCompany",
							"display": true,
							"tooltip": "Enter primary  Insurance comapany Name",
							"i18Key": "",
							"validations": {
								"required": false
							}
						},
						{
							"key": "zip_code",
							"display": true,
							"i18Key": "",
							"tooltip": "Enter valid Zipcode",
							"validations": {
								"required": true,
								"minLength": 5,
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							}
						},
						{
							"key": "age",
							"display": true,
							"i18Key": "",
							"tooltip": "Age",
							"validations": {
								"required": true,
								"pattern": "^([0-9]|[1-8][0-9]|9[0-9]|1[01][0-9]|120)?$"
							}
						},
						{
							"key": "tc_hippa",
							"default_value": false,
							"display": true,
							"i18Key": "Please select this option if you want to receive text message"
						}
					]
				}
			]
		},
		{
			"key": "screen0",
			"label": "CHEIF-COMPLAINT-SEARCH",
			"description": "1st optional screen in the appointment scheduling workflow.",
			"forms": [
				{
					"key": "triageInit",
					"label": "Search Information",
					"description": "Form to capture cheif complain, age, insurance and zipcode information",
					"fields": [
						{
							"key": "searchTerm",
							"display": true,
							"i18Key": "",
							"tooltip": "Search Visit Reason"
						},
						{
							"key": "insuranceCompany",
							"tooltip": "Enter primary  Insurance comapany Name",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": false
							}
						},
						{
							"key": "zip_code",
							"display": true,
							"i18Key": "",
							"tooltip": "Enter valid Zipcode"
						},
						{
							"key": "age",
							"display": true,
							"i18Key": "",
							"tooltip": "Age",
							"validations": {
								"required": true,
								"pattern": "^([0-9]|[1-8][0-9]|9[0-9]|1[01][0-9]|120)?$"
							}
						},
						{
							"key": "tc_hippa",
							"default_value": false,
							"display": true,
							"i18Key": "Please select this option if you want to receive text message"
						}
					]
				}
			]
		}
	],
	"dataConfigs": {
		"PATIENT_LANGUAGE_OPTIONS": [
			{
				"id": "1",
				"label": "English"
			},
			{
				"id": "2",
				"label": "Spanish"
			},
			{
				"id": "17",
				"label": "Other"
			}
		],
		"PROCESSING_MESSAGE_INTERVAL": {
			"time_in_sec_one": 2,
			"time_in_sec_two": 10
		}
	}
}'
where key ='SELF_DISPLAY_OPTIONS';

