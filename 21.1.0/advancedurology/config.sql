update system_config
set value = '{
	"screens": [
		{
			"key": "screen1",
			"label": "PINFO",
			"description": "1st screen in the appointment scheduling workflow.",
			"forms": [
				{
					"key": "patientInfo",
					"label": "Patient Information",
					"description": "Form to capture patient information",
					"fields": [
						{
							"key": "patient_fname",
							"display": true,
							"tooltip": "Patient''s First Name",
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							}
						},
						{
							"key": "patient_mname",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_lname",
							"display": true,
							"tooltip": "Patient''s Last Name",
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							}
						},
						{
							"key": "patient_dob_month",
							"display": true,
							"tooltip": "Patient''s Birth Month",
							"i18Key": "",
							"validations": {
								"required": true
							}
						},
						{
							"key": "patient_dob_day",
							"display": true,
							"tooltip": "Patient''s Birth Day",
							"i18Key": "",
							"validations": {
								"required": true
							}
						},
						{
							"key": "patient_dob_year",
							"display": true,
							"tooltip": "Patient''s Birth Year",
							"i18Key": "",
							"validations": {
								"required": true
							}
						},
						{
							"key": "patient_gender",
							"display": true,
							"tooltip": "Patient''s Gender",
							"i18Key": "",
							"validations": {
								"required": true
							}
						},
						{
							"key": "patient_hphone",
							"display": true,
							"tooltip": "Patient''s Home Phone",
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "\\d{10}$"
							}
						},
						{
							"key": "patient_email",
							"display": false,
							"tooltip": "Patient''s email",
							"i18Key": "",
							"validations": {
								"required": true,
								"email": true
							}
						},
						{
							"key": "patient_zip",
							"display": true,
							"tooltip": "Patient''s Zip code",
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "^[\\d]+$",
								"minLength": 5,
								"maxLength": 5
							}
						},
						{
							"key": "patient_language",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_mphone",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_address",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_city",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_state",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_ssn",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_primary_provider_name",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_primary_provider_id",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_primary_care_provider",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_text_notification",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "patient_email_notification",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "tc_hippa",
							"display": true,
							"i18Key": "",
							"validations": {
								"requiredTrue": true
							}
						},
						{
							"key": "captcha",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": false
							}
						},
						{
							"key": "tc_hippa",
							"display": false,
							"i18Key": "",
							"validations": {
								"requiredTrue": true
							}
					]
				}
			]
		},
		{
			"key": "screen2",
			"label": "IINFO",
			"description": "2nd screen in the appointment scheduling workflow.",
			"forms": [
				{
					"key": "insuranceInfo",
					"label": "Insurance Information",
					"description": "Form to capture Insurance information",
					"fields": [
						{
							"key": "primaryInsuranceCompany",
							"display": true,
							"tooltip": "Primary Insurance",
							"i18Key": "",
							"validations": {
								"required": true
							}
						},
						{
							"key": "primary_policy_id",
							"display": true,
							"tooltip": "Policy Id",
							"i18Key": "",
							"validations": {
								"pattern": "^[\\w\\d\\s\\-\\'']+$",
								"required": true
							}
						},
						{
							"key": "primary_group_number",
							"display": true,
							"tooltip": "Group Number",
							"i18Key": "",
							"validations": {
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							}
						},
						{
							"key": "primary_insurance_phone_no",
							"display": true,
							"tooltip": "Phone Number",
							"i18Key": "",
							"validations": {
								"required": false
							}
						},
						{
							"key": "secondary_insurance_option",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "secondaryInsuranceCompany",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "secondary_policy_id",
							"display": true,
							"i18Key": "",
							"validations": {
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							}
						},
						{
							"key": "secondary_group_number",
							"display": true,
							"i18Key": "",
							"validations": {
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							}
						},
						{
							"key": "secondary_insurance_phone_no",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "no_insurance_option",
							"display": false,
							"i18Key": ""
						},
						{
							"key": "work_insurance_option",
							"display": false,
							"i18Key": ""
						}
					]
				}
			]
		},
		{
			"key": "screen3",
			"label": "VISIT.REASON",
			"description": "3rd screen in the appointment scheduling workflow.",
			"forms": [
				{
					"key": "visitReason",
					"label": "Reason for visit",
					"description": "Form to capture reason for visit",
					"fields": [
						{
							"key": "reason",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": true
							}
						}
					]
				}
			]
		},
		{
			"key": "screen4",
			"label": "CLINICAL.DETAILS",
			"description": "4th screen in the appointment scheduling workflow.",
			"forms": [
				{
					"key": "clinicalDetails",
					"label": "Clinical Details",
					"description": "Form to capture clinical details requested by the provkeyer",
					"fields": []
				}
			]
		},
		{
			"key": "screen5",
			"label": "BOOKING",
			"description": "5th screen in the appointment scheduling workflow.",
			"forms": [
				{
					"key": "demographicData",
					"label": "Additional Patient Information ",
					"description": "Form to capture additional patient information",
					"fields": [
						{
							"key": "patient_fname",
							"display": true,
							"tooltip": "First Name",
							"disabled": true,
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": true
							}
						},
						{
							"key": "patient_lname",
							"tooltip": "Last Name",
							"display": true,
							"disabled": true,
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": true
							}
						},
						{
							"key": "patient_pname",
							"display": true,
							"tooltip": "P Name",
							"disabled": false,
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": false
							}
						},
						{
							"key": "patient_dob",
							"display": true,
							"tooltip": "Date Of Birth",
							"disabled": true,
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": true
							}
						},
						{
							"key": "patient_gender",
							"display": true,
							"tooltip": "Gender",
							"disabled": true,
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": true
							}
						},
						{
							"key": "patient_ssn",
							"display": false,
							"tooltip": "SSN",
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": true,
								"minLength": 9,
								"maxLength": 9
							}
						},
						{
							"key": "patient_language",
							"display": true,
							"tooltip": "Language",
							"formGroupName": "patientInfoGroup",
							"i18Key": "",
							"validations": {
								"required": true
							}
						},
						{
							"key": "patient_location",
							"display": true,
							"tooltip": "Location",
							"formGroupName": "patientAddressGroup",
							"i18Key": "",
							"validations": {
								"required": true
							}
						},
						{
							"key": "patient_suite",
							"display": true,
							"tooltip": "Suite",
							"formGroupName": "patientAddressGroup",
							"i18Key": ""
						},
						{
							"key": "patient_city",
							"display": true,
							"tooltip": "City",
							"formGroupName": "patientAddressGroup",
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							}
						},
						{
							"key": "patient_state",
							"display": true,
							"tooltip": "State",
							"formGroupName": "patientAddressGroup",
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							}
						},
						{
							"key": "patient_zip",
							"display": true,
							"tooltip": "Zipcode",
							"formGroupName": "patientAddressGroup",
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "^[\\d]+$",
								"minLength": 5
							}
						},
						{
							"key": "patient_mphone",
							"display": true,
							"tooltip": "Mobile Phone Number",
							"formGroupName": "patientContactGroup",
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "\\d{10}$"
							}
						},
						{
							"key": "patient_hphone",
							"display": true,
							"tooltip": "Home Phone Number",
							"formGroupName": "patientContactGroup",
							"i18Key": "",
							"validations": {
								"required": true,
								"pattern": "\\d{10}$"
							}
						},
						{
							"key": "patient_email",
							"display": true,
							"tooltip": "Email",
							"formGroupName": "patientContactGroup",
							"i18Key": "",
							"validations": {
								"required": true,
								"email": true
							}
						},
						{
							"key": "primary_provider",
							"display": true,
							"tooltip": "Primary Provider",
							"formGroupName": "providerInformationGroup",
							"autocomplete": true,
							"i18Key": "",
							"validations": {
								"required": true
							}
						},
						{
							"key": "referring_provider",
							"display": true,
							"tooltip": "Referring Provider",
							"formGroupName": "providerInformationGroup",
							"autocomplete": true,
							"i18Key": "",
							"validations": {
								"required": true
							}
						},
						{
							"key": "patient_text_notification",
							"display": true,
							"i18Key": "Please select this option if you want to receive text message"
						},
						{
							"key": "patient_email_notification",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "is_waitlist",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "is_demographics_update",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "is_insurance_update",
							"display": true,
							"i18Key": ""
						},
						{
							"key": "no_insurance_option",
							"display": false,
							"i18Key": ""
						}
					]
				},
				{
					"key": "feedback",
					"label": "Feedback",
					"description": "Form to capture user feedback",
					"fields": []
				}
			]
		},
		{
			"key": "screen6",
			"label": "CONFIRMATION",
			"description": "5th screen in the appointment scheduling workflow.",
			"display": {
				"maps": true
			}
		},
		{
			"key": "LandingPage",
			"label": "Main application search page",
			"description": "First page of self application to search cheif complain, insurance, zipcode, age",
			"forms": [
				{
					"key": "searchPage",
					"label": "Search Information",
					"description": "Form to capture cheif complain, age, insurance and zipcode information",
					"fields": [
						{
							"key": "searchTerm",
							"display": true,
							"i18Key": "",
							"tooltip": "Search Visit Reason",
							"validations": {
								"required": true
							}
						},
						{
							"key": "insuranceCompany",
							"display": true,
							"tooltip": "Enter primary  Insurance comapany Name",
							"i18Key": "",
							"validations": {
								"required": false
							}
						},
						{
							"key": "zip_code",
							"display": true,
							"i18Key": "",
							"tooltip": "Enter valid Zipcode",
							"validations": {
								"required": true,
								"minLength": 5,
								"pattern": "^[\\w\\d\\s\\-\\'']+$"
							}
						},
						{
							"key": "age",
							"display": true,
							"i18Key": "",
							"tooltip": "Age",
							"validations": {
								"required": true,
								"pattern": "^([0-9]|[1-8][0-9]|9[0-9]|1[01][0-9]|120)?$"
							}
						},
						{
							"key": "tc_hippa",
							"default_value": false,
							"display": true,
							"i18Key": "Please select this option if you want to receive text message"
						}
					]
				}
			]
		},
		{
			"key": "screen0",
			"label": "CHEIF-COMPLAINT-SEARCH",
			"description": "1st optional screen in the appointment scheduling workflow.",
			"forms": [
				{
					"key": "triageInit",
					"label": "Search Information",
					"description": "Form to capture cheif complain, age, insurance and zipcode information",
					"fields": [
						{
							"key": "searchTerm",
							"display": true,
							"i18Key": "",
							"tooltip": "Search Visit Reason"
						},
						{
							"key": "insuranceCompany",
							"tooltip": "Enter primary  Insurance comapany Name",
							"display": true,
							"i18Key": "",
							"validations": {
								"required": false
							}
						},
						{
							"key": "zip_code",
							"display": true,
							"i18Key": "",
							"tooltip": "Enter valid Zipcode"
						},
						{
							"key": "age",
							"display": true,
							"i18Key": "",
							"tooltip": "Enter patients Age",
							"validations": {
								"required": true,
								"pattern": "^([0-9]|[1-8][0-9]|9[0-9]|1[01][0-9]|120)?$"
							}
						},
						{
							"key": "tc_hippa",
							"default_value": false,
							"display": true,
							"i18Key": "Please select this option if you want to receive text message"
						}
					]
				}
			]
		}
	],
	"dataConfigs": {
		"PATIENT_LANGUAGE_OPTIONS": [
			{
				"id": "1",
				"label": "English"
			},
			{
				"id": "2",
				"label": "Spanish"
			},
			{
				"id": "17",
				"label": "Other"
			}
		],
		"PROCESSING_MESSAGE_INTERVAL": {
			"time_in_sec_one": 2,
			"time_in_sec_two": 10
		}
	}
}'
where value = 'SELF_DISPLAY_OPTIONS';



update system_config
set VALUE = '{
	"appointment_page": [
		{
			"desc": "urgent appointment button",
			"fid": "APPT001",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "exception appointment button",
			"fid": "APPT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "preferred provider autoselect",
			"fid": "APPT003",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Update preferred provider on Appt Confirmation",
			"fid": "APPT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Appt Insurance addition on Appt Confirmation popup",
			"fid": "APPT005",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "New Patient label in Provider listing in Appointments page",
			"fid": "APPT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Appointments/Patient page comments",
			"fid": "APPT007",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "35",
			"desc": "Default Findslot Range in Days",
			"fid": "APPT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "21",
			"desc": "Default Reduced Findslot Range in Days",
			"fid": "APPT009",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Removing Auto Populated Chief Complaint Notes - (set to true for auto populate)",
			"fid": "APPT017",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Do not carry forward previous appointment''s notes on rescheduling",
			"fid": "APPT018",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show/Hide REFERRING PROVIDER FAX NUMBER",
			"fid": "APPT015",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Mandate REFERRING PROVIDER FAX NUMBER",
			"fid": "APPT016",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show telehealth confirmation pop-up",
			"fid": "APPT024",
			"is_enabled": "false",
			"user_group": "*"
		}
	],
	"patient_page": [
		{
			"desc": "workers comp",
			"fid": "PT001",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Demographic Edit",
			"fid": "PT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Duplicate Patient Check",
			"fid": "PT003",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Self pay button",
			"fid": "PT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient status display",
			"fid": "PT005",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 1",
			"fid": "PT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 2",
			"fid": "PT007",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 3",
			"fid": "PT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Portal Invite dropdown option",
			"fid": "PT009",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Alerts dropdown option",
			"fid": "PT010",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Flags dropdown option",
			"fid": "PT011",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Care Team  dropdown option",
			"fid": "PT012",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Payment mode button patient card",
			"fid": "PT035",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Family size, income etc box - HJA specific for now",
			"fid": "PT013",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Referring provider display",
			"fid": "PT014",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "PCP display",
			"fid": "PT015",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to Text",
			"fid": "PT016",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to Call",
			"fid": "PT017",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to email",
			"fid": "PT018",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to language interpreter",
			"fid": "PT019",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Patient Alert",
			"fid": "PT020",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "ELFP specific alerts display on top",
			"fid": "PT021",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Allscript notes button on patient card",
			"fid": "PT027",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Check Insurance Eligibility in Insurance Screen",
			"fid": "PT022",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Get Insurance Details in Insurance Screen",
			"fid": "PT023",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Button for switching insurance priorities(lower,higher)",
			"fid": "PT024",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "10,41,42,43,44,45,46,47,50,51,52,53,54,56,57,58,59,60,61,62,63,64,66,69,70,71,72,73,74,79",
			"desc": "Comma separated list of Appointment statuses for which the Rescheduling button should be displayed on Patient''s screen",
			"fid": "PT028",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "10,41,42,43,44,45,46,47,50,51,52,53,54,56,57,58,59,60,61,62,63,64,66,69,70,71,72,73,74,79",
			"desc": "Comma separated list of Appointment statuses for which the Cancel button should be displayed on Patient''s screen",
			"fid": "PT029",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Add Associated Party button on Insurance Screen",
			"fid": "PT034",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show Appointment notes on details pop-up",
			"fid": "PT038",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Chief Complaint instead of Appointment Notes on expanding an appointment row on Patient screen",
			"fid": "PT039",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Search button on Search patient screen",
			"fid": "PT040",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Addition Description section on the appointment cancellation pop-up",
			"fid": "PT042",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"checkin_page": [
		{
			"default_value": "00",
			"desc": "Checkin - Default appointment statuses to search for initially in Today Appointments Screen",
			"fid": "CHECKIN001",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"default_theme_to_load": "rx"
}'
where key ='applicationUIConfig';


update system_config
set VALUE = '{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "Self Pay",
			"coverageDescription": "Please confirm you are agreeing to self-pay which indicates you are fully responsible for the charges incurred during your visits.",
			"check": false
		},
		{
			"id": "2",
			"coverageValue": "Commercial Insurance",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "3",
			"coverageValue": "Medicare",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "4",
			"coverageValue": "Medicaid",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "5",
			"coverageValue": "Worker''s Comp",
			"coverageDescription": "Please note workers comp will be treated as self-pay which indicates you are fully responsible for the charges incurred during your visits.",
			"check": false
		}
	],
	"showInsuranceForm": [
		"2",
		"3",
		"4"
	],
	"stopBooking": []
}'
where key ='INSURANCE_COVERAGE_CONFIG';

