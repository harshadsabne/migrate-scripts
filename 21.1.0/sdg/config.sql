update system_config
set value ='{
	"appointment_page": [
		{
			"desc": "urgent appointment button",
			"fid": "APPT001",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "exception appointment button",
			"fid": "APPT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "preferred provider autoselect",
			"fid": "APPT003",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Update preferred provider on Appt Confirmation",
			"fid": "APPT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Appt Insurance addition on Appt Confirmation popup",
			"fid": "APPT005",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "New Patient label in Provider listing in Appointments page",
			"fid": "APPT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Addition of Scheduler Name and timestamp in Appointments/Patient page comments",
			"fid": "APPT007",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "35",
			"desc": "Default Findslot Range in Days",
			"fid": "APPT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "21",
			"desc": "Default Reduced Findslot Range in Days",
			"fid": "APPT009",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Display of Recommended Provider not selected dropdown",
			"fid": "APPT010",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto-populate chief complaint in CC notes field",
			"fid": "APPT017",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Do not carry forward previous appointment''s notes on rescheduling",
			"fid": "APPT018",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Does EHR support multiple appointment notes structure?",
			"fid": "APPT019",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Does Integration Code prefer the first appointment note only for update in EHR?",
			"fid": "APPT020",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto add the R/S note from backend on rescheduling",
			"fid": "APPT021",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Add Visit reason/Chief complaint and Reason description to appointment note automatically",
			"fid": "APPT022",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show telehealth confirmation pop-up",
			"fid": "APPT024",
			"is_enabled": "false",
			"user_group": "*"
		}
	],
	"checkin_page": [
		{
			"default_value": "00,10,87,88,89",
			"desc": "Checkin - Default appointment statuses to search for in Today Appointments",
			"fid": "CHECKIN001",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Waiting Since,Checkin Progress and Next Action columns under Appointment table on Today Appointments Screen",
			"fid": "CHECKIN002",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Print button on Today Appointments Screen",
			"fid": "CHECKIN003",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Pre Survey button on Today Appointments Screen",
			"fid": "CHECKIN004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Actions button to bypass encounter on Today Appointments Screen",
			"fid": "CHECKIN005",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show/hide Insurance details below patient name",
			"fid": "CHECKIN006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show/hide Send message button on top bar after selection of appointment",
			"fid": "CHECKIN008",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"patient_page": [
		{
			"desc": "workers comp",
			"fid": "PT001",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Demographic Edit",
			"fid": "PT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Duplicate Patient Check",
			"fid": "PT003",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Self pay button",
			"fid": "PT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient status display",
			"fid": "PT005",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 1",
			"fid": "PT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 2",
			"fid": "PT007",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 3",
			"fid": "PT008",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Portal Invite dropdown option",
			"fid": "PT009",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alerts dropdown option",
			"fid": "PT010",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Flags dropdown option",
			"fid": "PT011",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Care Team  dropdown option",
			"fid": "PT012",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Family size, income etc box - HJA specific for now",
			"fid": "PT013",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Referring provider display",
			"fid": "PT014",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "PCP display",
			"fid": "PT015",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to Text",
			"fid": "PT016",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to Call",
			"fid": "PT017",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to email",
			"fid": "PT018",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Consent to language interpreter",
			"fid": "PT019",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Patient Alert",
			"fid": "PT020",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "ELFP specific alerts display on top",
			"fid": "PT021",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "1394,701,181,1454,1494,1334,91,92,3,1395,462,461,142,9,1396,464,463,521,522,581,1397,1166,1165,1398,1168,1167,1399,465,466,143,1400,482,481,6,89,361,96,10,1402,1164,1163,1403,483,484,141,1404,16,1401,1162,11,488,487,1161,1405,490,489,1406,1042,1041,1141,17,18,1121,1273,12,1407,492,491,1557,1555,1556,13,494,493,1408,1170,1169,1409,1172,1171,441,1021,381,14,283,901,61,5,1214,19,1554,941,15,981,201,82,282,121,284,285,421,182,1413,496,495,1574,1575,1576,7,183,1253,641,1173,1333,1474,1313,4",
			"desc": "Appointment types for which Add to waitlist button should be disabled",
			"fid": "PT022",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Allscripts Notes Button",
			"fid": "PT027",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Add Associated Party button on Insurance Screen",
			"fid": "PT034",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Payment mode button patient card",
			"fid": "PT035",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Insurance Priority Change Button",
			"fid": "PT024",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show Appointment notes on details pop-up",
			"fid": "PT038",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Search button on Search patient screen",
			"fid": "PT040",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Addition Description section on the appointment cancellation pop-up",
			"fid": "PT042",
			"is_enabled": "true",
			"user_group": "*"
		}
	]
}'
where key='applicationUIConfig';

update system_config
set Value='{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "I have insurance",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "2",
			"coverageValue": "No Insurance",
			"coverageDescription": "Self pay charges can be up to $200.00 up front out of pocket.",
			"check": false
		},
		{
			"id": "3",
			"coverageValue": "Workers Comp",
			"coverageDescription": "We are sorry but we cannot schedule a workers compensation appointment online right now. Please contact our scheduling department at 303.788.8888 and we will be happy to assist you.",
			"check": false
		}
	],
	"showInsuranceForm": [
		"1"
	],
	"stopBooking": [
		"2",
		"3"
	]
}'
where Key='INSURANCE_COVERAGE_CONFIG';

