
update system_config
set value ='{
	"patient_page": [
		{
			"fid": "PT001",
			"desc": "workers comp",
			"user_group": "*",
			"is_enabled": "true"
		},
		{
			"fid": "PT002",
			"desc": "Demographic Edit",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"fid": "PT003",
			"desc": "Duplicate Patient Check",
			"user_group": "*",
			"is_enabled": "true"
		},
		{
			"fid": "PT004",
			"desc": "Self pay button",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"fid": "PT005",
			"desc": "Patient status display",
			"user_group": "*",
			"is_enabled": "true"
		},
		{
			"fid": "PT006",
			"desc": "Patient Alert 1",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"fid": "PT007",
			"desc": "Patient Alert 2",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"fid": "PT008",
			"desc": "Patient Alert 3",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"fid": "PT009",
			"desc": "Patient Portal Invite dropdown option",
			"user_group": "*",
			"is_enabled": "true"
		},
		{
			"fid": "PT010",
			"desc": "Patient Alerts dropdown option",
			"user_group": "*",
			"is_enabled": "true"
		},
		{
			"fid": "PT011",
			"desc": "Patient Flags dropdown option",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"fid": "PT012",
			"desc": "Patient Care Team  dropdown option",
			"user_group": "*",
			"is_enabled": "true"
		},
		{
			"fid": "PT013",
			"desc": "Family size, income etc box - HJA specific for now",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"fid": "PT014",
			"desc": "Referring provider display",
			"user_group": "*",
			"is_enabled": "true"
		},
		{
			"fid": "PT015",
			"desc": "PCP display",
			"user_group": "*",
			"is_enabled": "true"
		},
		{
			"fid": "PT016",
			"desc": "Consent to Text",
			"user_group": "*",
			"is_enabled": "true"
		},
		{
			"fid": "PT017",
			"desc": "Consent to Call",
			"user_group": "*",
			"is_enabled": "true"
		},
		{
			"fid": "PT018",
			"desc": "Consent to email",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"fid": "PT019",
			"desc": "Consent to language interpreter",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"fid": "PT024",
			"desc": "Button for switching insurance priorities(lower,higher)",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"fid": "PT027",
			"desc": "Allscripts Notes",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"desc": "Add Associated Party button on Insurance Screen",
			"fid": "PT034",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Payment mode button patient card",
			"fid": "PT035",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "10,41,42,43,44,45,46,47,58,60,61,62,63,64,65,66,67,68,69,70,71,72,73",
			"desc": "Appointment Statuses to allow reschedule",
			"fid": "PT028",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "10,41,42,43,44,45,46,47,58,60,61,62,63,64,65,66,67,68,69,70,71,72,73",
			"desc": "Comma separated list of Appointment statuses for which the Cancel button should be displayed on Patient''s screen",
			"fid": "PT029",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Appointment notes on details pop-up",
			"fid": "PT038",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Search button on Search patient screen",
			"fid": "PT040",
			"is_enabled": "true",
			"user_group": "*"
		},
		
		{
			"desc": "Show Addition Description section on the appointment cancellation pop-up",
			"fid": "PT042",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"appointment_page": [
		{
			"fid": "APPT001",
			"desc": "urgent appointment button",
			"user_group": "*",
			"is_enabled": "true"
		},
		{
			"fid": "APPT002",
			"desc": "exception appointment button",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"fid": "APPT003",
			"desc": "preferred provider autoselect",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"fid": "APPT004",
			"desc": "Update preferred provider on Appt Confirmation",
			"user_group": "*",
			"is_enabled": "false"
		},
		{
			"fid": "APPT005",
			"desc": "Appt Insurance addition on Appt Confirmation popup",
			"user_group": "*",
			"is_enabled": "False"
		},
		{
			"fid": "APPT006",
			"desc": "New Patient label in Provider listing in Appointments page",
			"user_group": "*",
			"is_enabled": "true"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Appointments/Patient page comments",
			"fid": "APPT007",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Do not carry forward previous appointment''s notes on rescheduling",
			"fid": "APPT018",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Does EHR support multiple appointment notes structure? The overwrite_existing_note flag decides if the new note should be overwritten to the existing note instead of appending to it, for EHRs that support single note structure. Eg Centricity.",
			"fid": "APPT019",
			"is_enabled": "false",
			"user_group": "*",
			"overwrite_existing_note": "true"
		},
		{
			"desc": "Does Integration Code prefer the first appointment note only for update in EHR?",
			"fid": "APPT020",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto add the R/S note from backend on rescheduling",
			"fid": "APPT021",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Add Visit reason/Chief complaint and Reason description to appointment note automatically",
			"fid": "APPT022",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto-populate chief complaint in CC notes field",
			"fid": "APPT017",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show telehealth confirmation pop-up",
			"fid": "APPT024",
			"is_enabled": "false",
			"user_group": "*"
		}
	],
	"default_theme_to_load": "rx"
}'
where key='applicationUIConfig';


update system_config
set Value='{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "Private Insurance",
			"coverageDescription": "",
			"check": false
		},
		
		{
			"id": "2",
			"coverageValue": "Work Comp",
			"coverageDescription": "We''re sorry, but you cannot book an appointment with this selection. To schedule an appointment, please call our office at 404-255-3633 ",
			"check": false
		},
		
	],
	"showInsuranceForm": [],
	"stopBooking": [
		"2"
	]
}'
where Key='INSURANCE_COVERAGE_CONFIG';
