

update system_config
set value ='{
	"appointment_page": [
		{
			"desc": "urgent appointment button",
			"fid": "APPT001",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "exception appointment button",
			"fid": "APPT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "preferred provider autoselect",
			"fid": "APPT003",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Update preferred provider on Appt Confirmation",
			"fid": "APPT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Appt Insurance addition on Appt Confirmation popup",
			"fid": "APPT005",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "New Patient label in Provider listing in Appointments page",
			"fid": "APPT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Addition of Scheduler Name and timestamp in Appointments/Patient page comments",
			"fid": "APPT007",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "120",
			"desc": "Default Findslot Range in Days",
			"fid": "APPT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "30",
			"desc": "Default Reduced Findslot Range in Days",
			"fid": "APPT009",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Autoselect multiple linked PAs",
			"fid": "APPT014",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Auto-populate chief complaint in CC notes field",
			"fid": "APPT017",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Do not carry forward previous appointment''s notes on rescheduling",
			"fid": "APPT018",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Does EHR support multiple appointment notes structure?",
			"fid": "APPT019",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Does Integration Code prefer the first appointment note only for update in EHR?",
			"fid": "APPT020",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Auto add the R/S note from backend on rescheduling",
			"fid": "APPT021",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Add Visit reason/Chief complaint and Reason description to appointment note automatically",
			"fid": "APPT022",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show telehealth confirmation pop-up",
			"fid": "APPT024",
			"is_enabled": "false",
			"user_group": "*"
		}
	],
	"checkin_page": [
		{
			"default_value": "00,87,88,89",
			"desc": "Checkin - Default appointment statuses to search for initially in Today Appointments Screen",
			"fid": "CHECKIN001",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show ''Waiting Since'',''Checkin Progress'' and ''Next Action'' columns under Appointment table on Today Appointments Screen",
			"fid": "CHECKIN002",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show ''Print'' button on Today Appointments Screen",
			"fid": "CHECKIN003",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show ''Pre Survey'' button on Today Appointments Screen",
			"fid": "CHECKIN004",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"patient_page": [
		{
			"desc": "workers comp",
			"fid": "PT001",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Demographic Edit",
			"fid": "PT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Duplicate Patient Check",
			"fid": "PT003",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Self pay button",
			"fid": "PT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient status display",
			"fid": "PT005",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 1",
			"fid": "PT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 2",
			"fid": "PT007",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 3",
			"fid": "PT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Portal Invite dropdown option",
			"fid": "PT009",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alerts dropdown option",
			"fid": "PT010",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Flags dropdown option",
			"fid": "PT011",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Care Team  dropdown option",
			"fid": "PT012",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Family size, income etc box - HJA specific for now",
			"fid": "PT013",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "0,8B9B17CC-0,AE321F5F-D,45EA3D46-1,6300D237-2,B2D1AD2A-F,67F4991B-1,0E5EC43F-5,8644DA80-A,910A48FF-D,9DBE9A8A-C,6ADE65AB-6,A4BC5E3A-4,4CFCE023-2,A39B1ABE-C,F56FB63D-9,7F75361C-5,380E79FC-8,943E66E9-5,C8EDE18C-E,490936E9-6,552C9D47-9,B2054CFB-5,94C66E91-6,4BD4D6B8-F,5C690550-D,53EE3A33-E,09332FED-7,DC7C60A8-4,4AA39F23-B,03937B1C-7,BD30BE49-2,2DE37B97-F,DB98AD31-0,84732601-1,54A50796-4,66B6FDFB-5,3B8BCC09-6,38CA5FAF-B,E3DB7FBD-C,4B9937C1-4,1A743CE3-B,FEDF2ED6-5,C3C694AD-7,A553D08E-E,F4A01FCB-0,CA55AEDA-7,5ED60A37-8,9C22F602-A,7181E39C-A,5C80FEBD-2,F1DE5BB0-B,4B5BF78A-D,F64B29C1-4,CFF580A0-9,1EF52C29-5,85332EE9-0,64A3702A-6,0389744F-8,27ED3B5A-8,667D9075-3,7E10B02E-2,4A414970-0,EB5C2578-E,36E5BF53-F,6E2936FD-3,ABE128F7-5,1A0AB005-7,76593D7F-C,F59C667B-F,396B646A-4,ACF709A7-E,2E161C62-4,7C8D4780-2,4DA26460-1,AAE46BE1-9,BE1B88A5-0,F49F08FE-6,B020824D-F,4BFB8E16-6,661EE357-A,68076600-6,AE38BBDC-5,AB0E556E-5,2DAB3C21-1,AE06C08A-1,9FAE7EB0-8,D24095CC-8,0D67B522-E,E352F438-8,5590F0A6-0,E38C6B56-C,3928C266-9,3E26E8BC-2,9905A6DE-E,EA0A4CDA-B,7C8787BF-9,D00F65B4-0,4AC8EC0D-6,E25C8F9D-9,EEFFD0F9-0,2F3CD56A-C,2B0BBF02-A,DE6C8132-1,054ED25E-A,1F9FF4DC-3,D013059C-5,3EB3B34A-A,0DBF2966-3,CDACB63E-7,91CB4BAD-2,63019720-2,58E5ECE1-7,37B92CFF-F,3AE70A8A-4,0A657D0A-7,B3E570E1-7,4A788ABE-7,2329C8CE-6,E56B5F51-E,D6C0ACD4-5,DF2F85D2-8,3F188FAD-1,9970D31E-4,CE423463-3,63484FA8-4,E654E0E7-F,680F5B9C-A,78008D75-4,45735CDF-9,3B72ED87-B,A49ABEA7-0,1F0F7619-4,5EAD62BE-0,D36187E0-C,57C365E4-3,3E635A60-1,8AC2180F-9,51176601-1,5E4ED651-9,2F00ABC7-F,A1D07961-C,E714822A-C,C623A3D5-9,D46E8EE1-D,B06C6207-D,2B1F2FEF-D,67C77F2B-4,7C3C163C-B,B9220533-D,3E951F6A-A,A3EDF018-6,583B4E7A-C,859ACB8C-A,CE1053DA-F,BE308255-1,CF691C63-1,1F685AE4-0,D595748D-C,38102C21-8,90AE0018-0,B7FBD4E1-4,10BD2A38-2,D4A07B8D-3,18E6D4D4-C,F65C3CFC-A,A1641EDD-0,E6C3C5A1-2,F27185B9-B,E03C0D1B-3,F5EE4990-7,E20E3542-9,E59D89A7-D,DDBDDEDE-8,5EFE93CB-9,EFEEA213-F,991F31AE-2,E0600B2A-B,4BAB7DB4-6,08043F68-6,B60C347E-E,4B291479-7,812CE3EF-8,FD12D21A-4,CDB4CC08-6,9DA5C3EC-4,533264C7-7,E9A3D0E6-C,2762C4D3-8,C910AAE5-7,749B851B-B,BCDF5F57-0,9C130109-6,09D82785-6,A30AE693-A,8B858A6E-8,61F22C44-2,1D4713A4-4,C0C626B5-9,5DD5510D-B,BEBEAACF-2,736A67DD-1,EF1F8231-8,0ABBD96B-2,7D5F4C92-7,CF64CB80-1,EA50B889-0,1896D67E-A,91D820B0-0,4C01F384-2,DE9034E6-9,49F98BD8-9,0CD107D7-6,61794BF9-F,DE92B2F2-D,CFEE6AE8-6,C97B3815-E,D253E919-E,304C62BA-8,9A149509-3,7F4BC1E6-7,674956AF-F,8225C6BD-0,2B003C40-6,B4643D2A-C,CF3903B5-D,14CF0451-F,E7B7DDC6-7,F5F18144-0,B8AEA999-D,8525F76D-E,F6342657-2,965FEDF4-3,6D6940B1-A,ADB41E5D-E,30E1E97E-3,6549C099-8,4C9284C8-3,94FA10AE-1,E6B507E3-7,06C402C5-A,12ADC56D-3,CE149213-B,C6E40FFA-6,6534D30F-7,12263943-A,7AD9FC7A-E,58A3BE26-7,E209FE2E-4,887D3900-5,F1221EBF-D,B34A1769-A,ED42C335-E,E88E172A-2,C7BF61E6-D,0B81F7CD-2,DCBC6E6A-8,D223438A-5,DCE9E8FC-6,B421362D-F,40A16FA3-0,EDB9F21B-8,366F9C70-2,679D566A-9,4B4361A0-2,B607BC89-9,81D508F1-E,BAB10561-B",
			"desc": "Appointment types for which Add to waitlist button should be disabled",
			"fid": "PT022",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Insurance Priority Change Button",
			"fid": "PT024",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Allscripts Notes Button",
			"fid": "PT027",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Add Associated Party button on Insurance Screen",
			"fid": "PT034",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Payment mode button patient card",
			"fid": "PT035",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show Appointment notes on details pop-up",
			"fid": "PT038",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Search button on Search patient screen",
			"fid": "PT040",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Addition Description section on the appointment cancellation pop-up",
			"fid": "PT042",
			"is_enabled": "true",
			"user_group": "*"
		}
	]
}'
where key='applicationUIConfig';

update system_config
set Value='{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "I have insurance",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "2",
			"coverageValue": "No Insurance",
			"coverageDescription": "Need to pay $200.00.",
			"check": false
		},
		{
			"id": "3",
			"coverageValue": "Workers Comp/MVA",
			"coverageDescription": "We are sorry but we cannot schedule a workers compensation appointment online right now. Please contact our scheduling department at 904-398-7205 and we will be happy to assist you.",
			"check": false
		}
	],
	"showInsuranceForm": [
		"1"
	],
	"stopBooking": [
		"3"
	]
}'
where Key='INSURANCE_COVERAGE_CONFIG';

