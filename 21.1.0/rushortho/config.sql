update system_config
set value ='{
	"appointment_page": [
		{
			"desc": "urgent appointment button",
			"fid": "APPT001",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "exception appointment button",
			"fid": "APPT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "preferred provider autoselect",
			"fid": "APPT003",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Update preferred provider on Appt Confirmation",
			"fid": "APPT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "1",
			"desc": "Appt Insurance addition on Appt Confirmation popup",
			"fid": "APPT005",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "0",
			"desc": "New Patient label in Provider listing in Appointments page",
			"fid": "APPT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Appointments/Patient page comments",
			"fid": "APPT007",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "60",
			"desc": "Default Findslot Range in Days",
			"fid": "APPT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "21",
			"desc": "Default Reduced Findslot Range in Days",
			"fid": "APPT009",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "1",
			"desc": "Display of Recommended Provider not selected dropdown",
			"fid": "APPT010",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Automate multiple PA selection on selecting MD. If disabled, selection will be manual via a popup",
			"fid": "APPT014",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show telehealth confirmation pop-up",
			"fid": "APPT024",
			"is_enabled": "false",
			"user_group": "*"
		}
	],
	"patient_page": [
		{
			"desc": "workers comp",
			"fid": "PT001",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Demographic Edit",
			"fid": "PT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Duplicate Patient Check",
			"fid": "PT003",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Self pay button",
			"fid": "PT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient status display",
			"fid": "PT005",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 1",
			"fid": "PT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 2",
			"fid": "PT007",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 3",
			"fid": "PT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Portal Invite dropdown option",
			"fid": "PT009",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alerts dropdown option",
			"fid": "PT010",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Flags dropdown option",
			"fid": "PT011",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Care Team  dropdown option",
			"fid": "PT012",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Family size, income etc box - HJA specific for now",
			"fid": "PT013",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Referring provider display",
			"fid": "PT014",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "PCP display",
			"fid": "PT015",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to Text",
			"fid": "PT016",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to Call",
			"fid": "PT017",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to email",
			"fid": "PT018",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to language interpreter",
			"fid": "PT019",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Patient Alert",
			"fid": "PT020",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "ELFP specific alerts display on top",
			"fid": "PT021",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "1004,1005,1006,1007,101,102,1024,108,1104,1105,1106,1124,1144,1145,1146,1147,1148,1149,1150,121,123,136,147,202,203,221,23,24,244,25,26,27,28,288,289,29,290,291,292,293,294,295,296,297,304,325,326,33,34,344,345,346,364,365,37,38,384,40,404,42,424,43,44,445,46,464,465,466,47,484,49,504,505,506,544,545,546,547,548,56,565,57,584,585,586,587,588,59,60,604,605,606,607,61,624,644,66,664,1166,666,667,68,684,72,73,744,75,78,784,79,804,82,824,83,844,845,846,864,884,904,924,925,944,964,984,985,665,1044,1084,1085,1165,106,1045,1064,1125,1164,1184,1185,1204,1224,1225,1244,1264,1284",
			"desc": "Appointment types for which Add to waitlist button should be disabled",
			"fid": "PT022",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Get Insurance Details in Insurance Screen",
			"fid": "PT023",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Button for switching insurance priorities(lower,higher)",
			"fid": "PT024",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Display Allscripts Note Button",
			"fid": "PT027",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "10,85",
			"desc": "Comma separated list of Appointment statuses for which the Rescheduling button should be displayed on Patient''s screen",
			"fid": "PT028",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Bypass triage workflow at the time of appointment rescheduling",
			"fid": "PT032",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Bypass triage workflow at the time of appointment followup",
			"fid": "PT033",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Add Associated Party button on Insurance Screen",
			"fid": "PT034",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Payment mode button patient card",
			"fid": "PT035",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show Appointment notes on details pop-up",
			"fid": "PT038",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Search button on Search patient screen",
			"fid": "PT040",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Addition Description section on the appointment cancellation pop-up",
			"fid": "PT042",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"checkin_page": [
		{
			"default_value": "00",
			"desc": "Checkin - Default appointment statuses to search for initially in Today Appointments Screen",
			"fid": "CHECKIN001",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"default_theme_to_load": "rx"
}'
where key='applicationUIConfig';

update system_config
set Value='{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "I have insurance",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "2",
			"coverageValue": "No Insurance or Self-Pay",
			"coverageDescription": "Please note that payment for clinic services may range from $250-$500, with additional fees if fracture care is necessary. Payment will be due at the time of service.",
			"check": true
		},
		{
			"id": "3",
			"coverageValue": "Workers Compensation",
			"coverageDescription": "We’re sorry, but we can’t book your appointment online right now. Please call our office at (877)-632-6637 or fill out the form found on https://www.rushortho.com/workers-compensation and a member of our scheduling team will contact you to help schedule an appointment.",
			"check": true
		},
		{
			"id": "4",
			"coverageValue": "Motor Vehicle Accident or Personal Injury",
			"coverageDescription": "We’re sorry, but we can’t book your appointment online right now. Please call our office at (877)-632-6637 or fill out the form found on https://www.rushortho.com/make-an-appointment and a member of our scheduling team will contact you to help schedule an appointment.",
			"check": true
		},
		{
			"id": "5",
			"coverageValue": "Medicaid / HMO",
			"coverageDescription": "We’re sorry, but we can’t book your appointment online right now. Please call our office at (877)-632-6637 or fill out the form found on https://www.rushortho.com/make-an-appointment and a member of our scheduling team will contact you to help schedule an appointment.",
			"check": true
		}
	],
	"showInsuranceForm": [
		"1"
	],
	"stopBooking": [
		"3",
		"4",
		"5"
	]
}'
where Key='INSURANCE_COVERAGE_CONFIG';
