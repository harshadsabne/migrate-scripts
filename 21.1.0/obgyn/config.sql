update system_config
set value ='{
	"appointment_page": [
		{
			"desc": "urgent appointment button",
			"fid": "APPT001",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "exception appointment button",
			"fid": "APPT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "preferred provider autoselect",
			"fid": "APPT003",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Update preferred provider on Appt Confirmation",
			"fid": "APPT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Appt Insurance addition on Appt Confirmation popup",
			"fid": "APPT005",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "New Patient label in Provider listing in Appointments page",
			"fid": "APPT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "",
			"desc": "Auto addition of scheduler name and timestamp in Appointments/Patient page comments. Add default value of short for format first initial and last name",
			"fid": "APPT007",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "35",
			"desc": "Default Findslot Range in Days",
			"fid": "APPT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "21",
			"desc": "Default Reduced Findslot Range in Days",
			"fid": "APPT009",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "0",
			"desc": "Display of Recommended Provider not selected dropdown ( set default value to 1 for making it a required field )",
			"fid": "APPT010",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Display of Sequential Scheduling button on the Appointments page",
			"fid": "APPT011",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Display of Motor vehicle auto accident checkbox on appointment confirmation screen",
			"fid": "APPT012",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Display of Eliminated Slots button on appointment scheduling screen",
			"fid": "APPT013",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Automate multiple PA selection on selecting MD. If disabled, selection will be manual via a popup",
			"fid": "APPT014",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show/Hide REFERRING PROVIDER FAX NUMBER",
			"fid": "APPT015",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Mandate REFERRING PROVIDER FAX NUMBER",
			"fid": "APPT016",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Removing Auto Populated Chief Complaint Notes - (set to true for auto populate)",
			"fid": "APPT017",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Do not carry forward previous appointment''s notes on rescheduling",
			"fid": "APPT018",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Does EHR support multiple appointment notes structure?",
			"fid": "APPT019",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Does Integration Code prefer the first appointment note only for update in EHR?",
			"fid": "APPT020",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto add the R/S note from backend on rescheduling",
			"fid": "APPT021",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Add Visit reason/Chief complaint and Reason description to appointment note automatically",
			"fid": "APPT022",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show telehealth confirmation pop-up",
			"fid": "APPT024",
			"is_enabled": "false",
			"user_group": "*"
		}
	],
	"checkin_page": [
		{
			"default_value": "00",
			"desc": "Checkin - Default appointment statuses to search for initially in Today Appointments Screen",
			"fid": "CHECKIN001",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Waiting Since,Checkin Progress and Next Action columns under Appointment table on Today Appointments Screen",
			"fid": "CHECKIN002",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Print button on Today Appointments Screen",
			"fid": "CHECKIN003",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Pre Survey button on Today Appointments Screen",
			"fid": "CHECKIN004",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show Actions button to bypass encounter on Today Appointments Screen",
			"fid": "CHECKIN005",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show/hide Insurance details below patient name",
			"fid": "CHECKIN006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Checkin - Show/hide payment history button on patient info display pop-up",
			"fid": "CHECKIN007",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"patient_page": [
		{
			"desc": "workers comp",
			"fid": "PT001",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Demographic Edit",
			"fid": "PT002",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Duplicate Patient Check",
			"fid": "PT003",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Self pay button",
			"fid": "PT004",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient status display",
			"fid": "PT005",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 1",
			"fid": "PT006",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 2",
			"fid": "PT007",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Alert 3",
			"fid": "PT008",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Portal Invite dropdown option",
			"fid": "PT009",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Alerts dropdown option",
			"fid": "PT010",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Flags dropdown option",
			"fid": "PT011",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Patient Care Team  dropdown option",
			"fid": "PT012",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Family size, income etc box - HJA specific for now",
			"fid": "PT013",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Referring provider display",
			"fid": "PT014",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "PCP display",
			"fid": "PT015",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to Text",
			"fid": "PT016",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to Call",
			"fid": "PT017",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to email",
			"fid": "PT018",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Consent to language interpreter",
			"fid": "PT019",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Auto addition of scheduler name and timestamp in Patient Alert",
			"fid": "PT020",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "ELFP specific alerts display on top",
			"fid": "PT021",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Get Details button on Insurance Screen",
			"fid": "PT023",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Insurance Priority Up-Down buttons on Insurance Screen",
			"fid": "PT024",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Check Eligibility button on Insurance Screen",
			"fid": "PT025",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Search in PM system button on Search patient screen",
			"fid": "PT026",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Allscript notes button on patient card",
			"fid": "PT027",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "10",
			"desc": "Comma separated list of Appointment statuses for which the Rescheduling button should be displayed on Patient''s screen",
			"fid": "PT028",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "10",
			"desc": "Comma separated list of Appointment statuses for which the Cancel button should be displayed on Patient''s screen",
			"fid": "PT029",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "10",
			"desc": "Comma separated list of Appointments statuses for highlighting appointments as Active (Bold etc) in the appointments list in the Patient screen",
			"fid": "PT030",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"default_value": "10",
			"desc": "Comma separated list of Appointments statuses for which Edit functionality should be enabled in the Appointment Details Popup",
			"fid": "PT031",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Bypass triage workflow at the time of appointment rescheduling",
			"fid": "PT032",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Bypass triage workflow at the time of appointment followup",
			"fid": "PT033",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Add Associated Party button on Insurance Screen",
			"fid": "PT034",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Payment mode button patient card",
			"fid": "PT035",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Associated Party button on Patient Page Display",
			"fid": "PT037",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"desc": "Show Appointment notes on details pop-up",
			"fid": "PT038",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Chief Complaint instead of Appointment Notes on expanding an appointment row on Patient screen",
			"fid": "PT039",
			"is_enabled": "false",
			"user_group": "*"
		},
		{
			"default_value": "1,2,21,22,23,25,26,27,28,29,30,31,32,35,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,82,83,84,102,121,181,201,241,261,281,341,342,361,401,522,542,563,564,582,602,622,662,663,664,682,683,684,685,702,703,722,742,762,763,764,803,842,864,902,903",
			"desc": "Appointment types for which Add to waitlist button should be disabled",
			"fid": "PT022",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Patient Search button on Search patient screen",
			"fid": "PT040",
			"is_enabled": "true",
			"user_group": "*"
		},
		{
			"desc": "Show Addition Description section on the appointment cancellation pop-up",
			"fid": "PT042",
			"is_enabled": "true",
			"user_group": "*"
		}
	],
	"default_theme_to_load": "rx",
	"dashin_theme": {
		"primary_color": "#58919B",
		"secondary_color": "#ea9e1D",
		"primary_text_color": "white",
		"secondary_text_color": "black",
		"completed_form_color": "green",
		"remaining_form_color": "yellow"
	}
}'
WHERE key='applicationUIConfig';

update system_config
set Value='{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "United HealthOne",
			"
			": "This plan has limited routine coverage. Patient may be self pay for annuals, IUD checks, or anything routine. We recommend calling your plan prior to appointment",
			"check": false
		},
		{
			"id": "2",
			"coverageValue": "Aetna",
			"coverageDescription": "Check card for Seton Health Alliance. We recommend calling the number on your insurance card prior to your appointment to confirm your appointment will be covered",
			"check": false
		},
		{
			"id": "3",
			"coverageValue": "Medicare",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "4",
			"coverageValue": "Medicaid",
			"coverageDescription": "If you have Dell Children''s Medicaid and think you are pregnant, pregnancy services are not covered. Please contact Dell Children''s Medicaid for other Medicaid options",
			"check": false
		},
		{
			"id": "5",
			"coverageValue": "Self Pay",
			"coverageDescription": "$300 deposit due upon arrival or $500 deposit due upon arrival for pregnant patients.",
			"check": true
		},
		{
			"id": "6",
			"coverageValue": "Blue Cross",
			"coverageDescription": "",
			"check": false
		},
		{
			"id": "7",
			"coverageValue": "Other Insurance",
			"coverageDescription": "",
			"check": false
		}
	],
	"showInsuranceForm": [
		"1",
		"2",
		"3",
		"4",
		"6",
		"7"
	],
	"stopBooking": []
}'
where Key='INSURANCE_COVERAGE_CONFIG';

