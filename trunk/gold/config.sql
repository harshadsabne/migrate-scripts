
update
	public.system_config
set
	value = '10,'
	where
	"key" = 'telehealthApptTypeIds';

UPDATE public.system_config
SET value='{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "This is work related condition, covered by workers compensation plan",
			"coverageDescription": "Additional notes for work comp",
			"check": false
		},
		{
			"id": "2",
			"coverageValue": "Will pay cash and not use insurance",
			"coverageDescription": "You will be charged a fee of $200 for new appointment",
			"check": false
		},
		{
			"id": "3",
			"coverageValue": "Will use health insurance",
			"coverageDescription": "Clinic does not accept XYZ plan",
			"check": false
		}
	],
	"showInsuranceForm": [
		"3"
	],
	"stopBooking": [
		""
	]
}'
WHERE "key"='INSURANCE_COVERAGE_CONFIG';
	
	
update
	public.system_code_detail
set
	system_code_value_desc = 'Spanish',	
	flag2 = 1
	where
	system_code_id = '1100'
	and system_code_type = '04'
	and system_code_value = 'es';

update
	public.system_code_detail
set
	flag2 = 1,
	is_active = 'Y'
where
	system_code_id = '1100'
	and system_code_type = '04'
	and system_code_value = 'en';
	
update
	public.system_config
set
	value = 'False'
	where
	"key" = 'RECAPTCHA_ENABLED';
	
